const actionsObj = require(`../actions/actions.js`);

class Routes {

    constructor(app) {
        // console.log("app",app)
        this.app = app;
        this.actionsInstance = new actionsObj(app);
    }
    init ()  {
        this.app.express.get(`/v1/login`, async (request, response) => {
            this.actionsInstance.login(request.query);
            response.send({test: "okay"});
        });
    }
}

module.exports = Routes;
