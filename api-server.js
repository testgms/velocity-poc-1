const promise = require('bluebird')
let request = require('request-promise');
request = request.defaults({ jar: true });
require('dotenv').config();
let bodyParser = require('body-parser');
const RouterActions = require('./source/routes/routes.js');

let express = require('express');
let expressValue = express();
expressValue = promise.promisifyAll(expressValue);

// const conf = require("./conf/"+ process.env.env+".js");

let app = {};
app['express'] = expressValue;
app['request'] = request;
app.express.use(bodyParser());
// app['conf'] = conf;

app.express.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});
try {
    let apiInfo = {
        host: process.env.serverHost,
        port: process.env.apiPort,
        routes: {
            cors: true
        }
    };
    expressValue.listen(apiInfo);
    console.log('Express Connection Initialized : ' + process.env.serverHost+':'+process.env.apiPort);
} catch (e) {
    console.log('Error @ Express Connection Initialization : ' + e)
}
class ApiServer {
  constructor () {
    const routesActionObject = new RouterActions(app);
    routesActionObject.init()
  }
}


let serverObject = new ApiServer();
module.exports = ApiServer;




